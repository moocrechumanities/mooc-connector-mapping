package com.mooc.connector.mapping.infrastructure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExternalProperties {
	
	@Value("${solr.connector.service.search.base.url}")
	private String solrBaseURL;
	
	@Value("${mooc-humanities-connector-mapping.api.key}")
	private String apiKey;

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getSolrBaseURL() {
		return solrBaseURL;
	}

	public void setSolrBaseURL(String solrBaseURL) {
		this.solrBaseURL = solrBaseURL;
	}
	

}
