package com.mooc.connector.mapping.infrastructure.repository;

import java.util.Set;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mooc.connector.mapping.application.Model.CourseModel;

public interface CourseRepository extends MongoRepository<CourseModel, String> {

	@Query("{ $and:[{'id' : ?0} , {'learnerStyle' : ?1} ,{ 'accent': ?2}] }")
	public CourseModel findAllExactlyMatchedCourses(String id, String learnerStyle, String accent);

	@Query("{ $and:[{'id' : ?0} , {'learnerStyle' : ?1 }]}")
	public CourseModel findAllLearnerStyleMatchedCourses(String id, String learnerStyle);

	@Query("{ $and:[{ 'id' : ?0} , {'accent' : ?1 }]}")
	public CourseModel findAllaccentMatchedCourses(String id, String accent);

	@Query("{ 'id' : ?0 }")
	public CourseModel findAllsearchResultMatchedCourses(String id);

	@Query("{ 'learnerStyle' : {$regex : '.*?0.*' }}")
	public Set<CourseModel> findAllCourseByLearnerStyle(String learnerStyle);

}
