package com.mooc.connector.mapping.infrastructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.mooc.connector.mapping.application.Model.UserModel;

public interface UserRepository extends MongoRepository<UserModel, String> {

}
 