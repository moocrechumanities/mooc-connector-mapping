package com.mooc.connector.mapping.application.transformer;

import java.util.Set;

import org.springframework.stereotype.Service;

import com.mooc.connector.mapping.application.DTO.MoocConnectorErrorResponseDTO;
import com.mooc.connector.mapping.application.DTO.MoocConnectorSearchResultMappingDTO;
import com.mooc.connector.mapping.application.DTO.MoocConnectorSuccessResponseDTO;
import com.mooc.connector.mapping.application.DTO.Solr.SolrConnectorResultResponseDTO;
import com.mooc.connector.mapping.application.Model.CourseModel;

@Service
public class MoocMappingTransformer {

	public MoocConnectorSearchResultMappingDTO toSearchResultMappingResponseDTO(SolrConnectorResultResponseDTO result,
			CourseModel course, int indexValue) {

		MoocConnectorSearchResultMappingDTO searchResultMappingResponseDTO = new MoocConnectorSearchResultMappingDTO();

		searchResultMappingResponseDTO.setCourseId(course.getId());
		searchResultMappingResponseDTO.setCourseTitle(course.getTitle());
		searchResultMappingResponseDTO.setLearnerStyle(course.getLearnerStyle());
		searchResultMappingResponseDTO.setQuizWeight(course.getQuizWeight());
		searchResultMappingResponseDTO.setPdfWeight(course.getPdfWeight());
		searchResultMappingResponseDTO.setMatchedIndex(indexValue);
		searchResultMappingResponseDTO.setCourseUrl(course.getUrl());
		searchResultMappingResponseDTO.setSource(course.getSource());
		searchResultMappingResponseDTO.setData(result);

		return searchResultMappingResponseDTO;
	}

	public MoocConnectorSuccessResponseDTO toSuccessResponse(Set<MoocConnectorSearchResultMappingDTO> results) {

		MoocConnectorSuccessResponseDTO response = new MoocConnectorSuccessResponseDTO();

		response.setStatus("SUCCESS");
		response.setData(results);

		return response;
	}

	public MoocConnectorErrorResponseDTO toErrorResponse(String Message) {

		MoocConnectorErrorResponseDTO response = new MoocConnectorErrorResponseDTO();

		response.setStatus("ERROR");
		response.setMessage(Message);

		return response;
	}

}
