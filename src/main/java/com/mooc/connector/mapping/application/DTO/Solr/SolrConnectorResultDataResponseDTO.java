package com.mooc.connector.mapping.application.DTO.Solr;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SolrConnectorResultDataResponseDTO {

	private List<SolrConnectorResultResponseDTO> data;

	public List<SolrConnectorResultResponseDTO> getData() {
		return data;
	}

	public void setData(List<SolrConnectorResultResponseDTO> data) {
		this.data = data;
	}
	
}
