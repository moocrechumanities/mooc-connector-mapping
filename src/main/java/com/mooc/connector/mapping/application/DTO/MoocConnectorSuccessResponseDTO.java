package com.mooc.connector.mapping.application.DTO;

import java.util.Set;

public class MoocConnectorSuccessResponseDTO {

	private String status;

	private Set<MoocConnectorSearchResultMappingDTO> data;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<MoocConnectorSearchResultMappingDTO> getData() {
		return data;
	}

	public void setData(Set<MoocConnectorSearchResultMappingDTO> data) {
		this.data = data;
	}


}
