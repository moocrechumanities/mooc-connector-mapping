package com.mooc.connector.mapping.application;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.mapping.application.DTO.MoocConnectorSearchResultMappingDTO;
import com.mooc.connector.mapping.application.transformer.MoocMappingTransformer;
import com.mooc.connector.mapping.domain.search.SearchServiceImpl;
import com.mooc.connector.mapping.infrastructure.ExternalProperties;

@Service
public class SolrConnectorApplicationService {

	@Autowired
	SearchServiceImpl searchServiceImpl;

	@Autowired
	MoocMappingTransformer transformer;

	@Autowired
	ExternalProperties exterrnalProperties;

	public Object searchResultApplicationService(String data, String learnerStyle, String accent, String apiKey) {

		if (validateApiKey(apiKey)) {

			Set<MoocConnectorSearchResultMappingDTO> resultSet = searchServiceImpl.mapping(data, learnerStyle, accent);
			return transformer.toSuccessResponse(resultSet);

		} else {

			return transformer.toErrorResponse("YOU PROVIDED API KEY IS NOT VALID");

		}

	}

	private boolean validateApiKey(String apiKey) {

		if (apiKey.equals(exterrnalProperties.getApiKey())) {
			return true;
		} else {
			return false;
		}

	}

}
