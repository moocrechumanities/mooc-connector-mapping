package com.mooc.connector.mapping.application.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "courses")
public class CourseModel {

	@Id
	private String _id;
	private String id;
	private String title;
	private String learnerStyle;
	private String quizWeight;
	private String pdfWeight;
	private String accent;
	private String source;
	private String url;

	public CourseModel(String id, String title, String learnerStyle, String pdfWeight, String quizWeight,
			String accent, String url) {

		this.id = id;
		this.title = title;
		this.learnerStyle = learnerStyle;
		this.quizWeight = quizWeight;
		this.pdfWeight = pdfWeight;
		this.accent = accent;
		this.url = url;

	}
	

	public String get_id() {
		return _id;
	}

	public String getQuizWeight() {
		return quizWeight;
	}

	public void setQuizWeight(String quizWeight) {
		this.quizWeight = quizWeight;
	}

	public String getPdfWeight() {
		return pdfWeight;
	}

	public void setPdfWeight(String pdfWeight) {
		this.pdfWeight = pdfWeight;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLearnerStyle() {
		return learnerStyle;
	}

	public void setLearnerStyle(String learnerStyle) {
		this.learnerStyle = learnerStyle;
	}

	public String getAccent() {
		return accent;
	}

	public void setAccent(String accent) {
		this.accent = accent;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}

}
