package com.mooc.connector.mapping.application;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.mapping.application.transformer.MoocMappingTransformer;
import com.mooc.connector.mapping.domain.course.CourseServiceImpl;
import com.mooc.connector.mapping.infrastructure.ExternalProperties;

import ch.qos.logback.classic.Logger;

@Service
public class CourseApllicationService {

	private Logger logger = (Logger) LoggerFactory.getLogger(CourseApllicationService.class);

	@Autowired
	MoocMappingTransformer transformer;

	@Autowired
	ExternalProperties exterrnalProperties;

	@Autowired
	CourseServiceImpl courseServiceImpl;

	public Object getAllCourse(String learnerStyle, String apiKey) {

		if (validateApiKey(apiKey)) {

			logger.info("START RETRIEVING LEARNER STYLE MAPPED COURSES");
			return courseServiceImpl.getAllCourses(learnerStyle);

		} else {

			return transformer.toErrorResponse("YOU PROVIDED API KEY IS NOT VALID");

		}

	}

	private boolean validateApiKey(String apiKey) {

		if (apiKey.equals(exterrnalProperties.getApiKey())) {
			return true;
		} else {
			return false;
		}

	}

}
