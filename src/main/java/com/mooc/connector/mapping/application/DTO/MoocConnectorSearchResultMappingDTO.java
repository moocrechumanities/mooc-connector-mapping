package com.mooc.connector.mapping.application.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mooc.connector.mapping.application.DTO.Solr.SolrConnectorResultResponseDTO;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MoocConnectorSearchResultMappingDTO {

	private String courseId;
	private String courseTitle;
	private String learnerStyle;
	private String quizWeight;
	private String pdfWeight;
	private String courseUrl;
	private int matchedIndex;
	private String source;
	private SolrConnectorResultResponseDTO data;

	public String getQuizWeight() {
		return quizWeight;
	}

	public void setQuizWeight(String quizWeight) {
		this.quizWeight = quizWeight;
	}

	public String getPdfWeight() {
		return pdfWeight;
	}

	public void setPdfWeight(String pdfWeight) {
		this.pdfWeight = pdfWeight;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	public int getMatchedIndex() {
		return matchedIndex;
	}

	public void setMatchedIndex(int matchedIndex) {
		this.matchedIndex = matchedIndex;
	}

	public SolrConnectorResultResponseDTO getData() {
		return data;
	}

	public void setData(SolrConnectorResultResponseDTO data) {
		this.data = data;
	}

	public String getLearnerStyle() {
		return learnerStyle;
	}

	public void setLearnerStyle(String learnerStyle) {
		this.learnerStyle = learnerStyle;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseUrl() {
		return courseUrl;
	}

	public void setCourseUrl(String courseUrl) {
		this.courseUrl = courseUrl;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

}
