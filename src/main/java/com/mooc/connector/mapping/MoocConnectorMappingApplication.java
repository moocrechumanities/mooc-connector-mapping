package com.mooc.connector.mapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoocConnectorMappingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoocConnectorMappingApplication.class, args);
	}

}
