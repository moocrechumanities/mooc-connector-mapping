package com.mooc.connector.mapping.domain.search;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mooc.connector.mapping.application.DTO.Solr.SolrConnectorResultDataResponseDTO;
import com.mooc.connector.mapping.infrastructure.ExternalProperties;

import ch.qos.logback.classic.Logger;

@Service
public class SolrConnectorIntegrationService {

	private Logger logger = (Logger) LoggerFactory.getLogger(SolrConnectorIntegrationService.class);

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	ExternalProperties exterrnalProperties;

	public SolrConnectorResultDataResponseDTO getResultfromSolr(String data) {

		logger.info("starter");

		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		HttpEntity<String> request = new HttpEntity<String>(headers);

		ResponseEntity<String> response = null;

		String url = exterrnalProperties.getSolrBaseURL() + "?data=" + data;

		logger.info("URL : " + url);

		try {

			logger.info("START RETRIEVING SOLR DATA : IN PROGRESS");

			response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);

			logger.info("RESPONSE : " + response);

		} catch (RestClientException e) {

			logger.info("START RETRIEVING SOLR DATA : CRASHED");

			throw new RestClientException("DATA PROCCESSING EXCEPTION");

		}

		return (response != null) ? convertJSONToObject(response, SolrConnectorResultDataResponseDTO.class) : null;
	}

	public <T> T convertJSONToObject(ResponseEntity<String> response, Class<T> responseType) {

		T t = null;

		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {

			t = objectMapper.readValue(response.getBody().toString(), responseType);

		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return t;
	}

	public <T> String convertObjectToJson(T t) {

		String output = null;
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			output = objectMapper.writeValueAsString(t);
		} catch (JsonProcessingException e) {
			logger.error(e.getMessage(), e);
		}

		return output;
	}
}
