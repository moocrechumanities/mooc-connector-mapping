package com.mooc.connector.mapping.domain.course;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.mapping.application.Model.CourseModel;
import com.mooc.connector.mapping.infrastructure.repository.CourseRepository;

@Service
public class CourseServiceImpl {

	@Autowired
	CourseRepository courseRepository;

	public Set<CourseModel> getAllCourses(String learnerStyle) {

		return courseRepository.findAllCourseByLearnerStyle(learnerStyle);

	};

}
