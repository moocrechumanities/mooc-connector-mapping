package com.mooc.connector.mapping.domain.search;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mooc.connector.mapping.application.DTO.MoocConnectorSearchResultMappingDTO;
import com.mooc.connector.mapping.application.DTO.Solr.SolrConnectorResultResponseDTO;
import com.mooc.connector.mapping.application.Model.CourseModel;
import com.mooc.connector.mapping.application.transformer.MoocMappingTransformer;
import com.mooc.connector.mapping.infrastructure.repository.CourseRepository;

import ch.qos.logback.classic.Logger;

@Service
public class SearchServiceImpl implements SearchServie {

	private Logger logger = (Logger) LoggerFactory.getLogger(SearchServiceImpl.class);

	@Autowired
	SolrConnectorIntegrationService solrConnectorIntegrationService;

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	MoocMappingTransformer solrConnectorServiceTransformer;

	@Override
	public Set<MoocConnectorSearchResultMappingDTO> mapping(String data, String learnerStyle, String accent) {

		Set<MoocConnectorSearchResultMappingDTO> SerachResultSet = new HashSet<MoocConnectorSearchResultMappingDTO>();

		List<SolrConnectorResultResponseDTO> solrConnectorResultList = solrConnectorIntegrationService
				.getResultfromSolr(data).getData();

		logger.info(solrConnectorResultList.size() + "RESULT FOUND ");

		for (SolrConnectorResultResponseDTO result : solrConnectorResultList) {

			CourseModel ExactlyMatchedCourse = courseRepository.findAllExactlyMatchedCourses(result.getCourseId(),
					learnerStyle, accent);
			CourseModel learnerStyleMatchedCourse = courseRepository
					.findAllLearnerStyleMatchedCourses(result.getCourseId(), learnerStyle);
			CourseModel accentMatchedCourse = courseRepository.findAllaccentMatchedCourses(result.getCourseId(),
					accent);
			CourseModel searchResultMatchedCourse = courseRepository
					.findAllsearchResultMatchedCourses(result.getCourseId());

			if (!((ExactlyMatchedCourse) == null)) {

				int indexValue = 0;

				MoocConnectorSearchResultMappingDTO searchResultMappedResponseDTO = solrConnectorServiceTransformer
						.toSearchResultMappingResponseDTO(result, ExactlyMatchedCourse, indexValue);

				SerachResultSet.add(searchResultMappedResponseDTO);

				logger.info(searchResultMappedResponseDTO.getCourseId() + "MATCHED WITH 0th INDEX");

			} else if (!((learnerStyleMatchedCourse) == null)) {

				int indexValue = 1;

				MoocConnectorSearchResultMappingDTO searchResultMappedResponseDTO = solrConnectorServiceTransformer
						.toSearchResultMappingResponseDTO(result, learnerStyleMatchedCourse, indexValue);

				SerachResultSet.add(searchResultMappedResponseDTO);

				logger.info(searchResultMappedResponseDTO.getCourseId() + "MATCHED WITH 1st INDEX");
				
			} else if (!((accentMatchedCourse) == null)) {

				int indexValue = 2;

				MoocConnectorSearchResultMappingDTO searchResultMappedResponseDTO = solrConnectorServiceTransformer
						.toSearchResultMappingResponseDTO(result, accentMatchedCourse, indexValue);

				SerachResultSet.add(searchResultMappedResponseDTO);
				
				logger.info(searchResultMappedResponseDTO.getCourseId() + "MATCHED WITH 2nd INDEX");

			} else if (!((searchResultMatchedCourse) == null)) {

				int indexValue = 3;

				MoocConnectorSearchResultMappingDTO searchResultMappedResponseDTO = solrConnectorServiceTransformer
						.toSearchResultMappingResponseDTO(result, searchResultMatchedCourse, indexValue);

				SerachResultSet.add(searchResultMappedResponseDTO);
				
				logger.info(searchResultMappedResponseDTO.getCourseId() + "MATCHED WITH 3rd INDEX");

			}
		}

		return SerachResultSet;
	}

}
