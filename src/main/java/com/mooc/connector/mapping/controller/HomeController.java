package com.mooc.connector.mapping.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String homeController() {
		return "WELCOME TO MAPPING SERVICE";
	}

}
