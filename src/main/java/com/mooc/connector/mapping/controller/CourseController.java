package com.mooc.connector.mapping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mooc.connector.mapping.application.CourseApllicationService;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class CourseController {

	@Autowired
	CourseApllicationService courseApllicationService;

	@RequestMapping(value = "/course", method = RequestMethod.GET)
	public Object search(@RequestParam(name = "learnerStyle") String learnerStyle,
			@RequestHeader(name = "API-KEY") String apiKey) {

		return courseApllicationService.getAllCourse(learnerStyle, apiKey);

	}

}
