package com.mooc.connector.mapping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mooc.connector.mapping.application.SolrConnectorApplicationService;

@RestController
public class SearchController {

	@Autowired
	SolrConnectorApplicationService SolrConnectorApplicationService;

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public Object search(@RequestParam(name = "data") String data,
			@RequestParam(name = "learnerStyle") String learnerStyle, @RequestParam(name = "accent") String accent,
			@RequestHeader(name = "API-KEY") String apiKey) {

		return SolrConnectorApplicationService.searchResultApplicationService(data, learnerStyle, accent, apiKey);

	}

}
